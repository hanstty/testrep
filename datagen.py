import random
import time
import string
import datetime


class DataGen:

    # generate current time difference
    minustime = random.uniform(500000, 1750000)

    def genipv4(self):
        ip = '{}.{}.{}.{}'.format(
            *__import__('random').sample(range(0, 255), 4))
        return ip

    def genuser(self):
        names = ["Bob", "Doe", "Mary", "Alice",
                 "Joe", "Andy", "Thomas", "Will", "Emma"]
        user = random.choice(names)
        return user

    def genemail(self):
        domains = ["yaho", "gugl", "offline", "mail", "tails"]
        mdomains = [".net", ".com", "co.uk", ".gov", ".edu"]
        email = str(self.genuser()).lower() + str(random.randint(1, 234)) + \
            "@" + random.choice(domains) + random.choice(mdomains)
        return email

    def gencode(self):
        codes = [100, 200, 201, 202, 204, 300, 301, 302, 303, 307, 308, 400, 401, 402, 403, 404, 405, 500, 501, 502,
                 503, 504, 505]
        code = random.choice(codes)
        return code

    def gencommand(self):
        commands = ["GET", "PUT", "POST", "DELETE"]
        command = random.choice(commands)
        return command

    def gentimestamp(self):
        actualtime = time.time()
        self.minustime -= 1
        fakestamp = (datetime.datetime.fromtimestamp(
            actualtime - abs(self.minustime)).strftime('%Y-%m-%d %H:%M:%S'))
        return fakestamp

    def genurl(self):
        fakeurl = "/" + ''.join(random.choice(string.ascii_lowercase) for _ in range(int(random.uniform(3, 10)))) + \
                  "/" + ''.join(random.choice(string.ascii_lowercase) for _ in range(int(random.uniform(5, 15)))) + \
                  "/" + ''.join(random.choice(string.ascii_lowercase)
                                for _ in range(int(random.uniform(3, 5)))) + "/"
        return fakeurl

    def gendata(self):
        data = ''.join(random.choice(string.ascii_uppercase +
                                     string.digits) for _ in range(10))
        return data
