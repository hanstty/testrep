from unittest import TestCase
from datagen import DataGen
import re


class TestDataGen(TestCase):
    def test_genCommand(self):
        gen = DataGen()
        value = (gen.gencommand())
        approvedValues = ["GET", "PUT", "POST", "DELETE"]

        if value not in approvedValues:
            self.fail()

    def test_gencode(self):
        gen = DataGen()
        value = (gen.gencode())
        approvedValues = [100, 200, 201, 202, 204, 300, 301, 302, 303, 307, 308, 400, 401, 402, 403, 404, 405, 500, 501, 502,
                          503, 504, 505]

        if value not in approvedValues:
            self.fail()

    def test_genipv4(self):
        gen = DataGen()
        value = (gen.genipv4())

        if ([0 <= int(x) < 256 for x in re.split('\.', re.match(r'^\d+\.\d+\.\d+\.\d+$', value).group(0))].count(True) == 4) is True:
            pass
        else:
            self.fail()

    def test_genname(self):
        gen = DataGen()
        value = (gen.genuser())

        if isinstance(value, str) is True & value[0].isupper() is False:
            self.fail()

    def fail(self):
        self.fail()
