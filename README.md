#readme

This is a simple logline generator if you need sample logs.
Run it via

"python main.py logfilename 10000"
Where 10000 is the amount of lines you want and 4 is the amount of columns you want and logfilename is the name of the log file.

If you specify a file what already exists, lines are appended to it.

Please note: Names / URLs / loglines are not to meant to make any sense.
Everything is weak random intentionally, there was no need to create a secureRNG version of this.

Log format will look like this:

timestamp ip name random string(10char) email resourcURL command(get/post) response code
e.g.
2017-12-28 09:16:14   250.23.94.207  Andy    UZ9B4A1OKZ    doe179@gugl.gov    /test/fake/no    POST  500

See more in sample.log

