from __future__ import unicode_literals
from __future__ import print_function

from platform import python_version
import sys
import argparse

from datagen import DataGen

genekas = DataGen()


def checkarg():

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", help="Specify output filename")
    parser.add_argument("-l", "--lines", type=int,
                        help="Speciry number of lines")
    parser.add_argument("-c", "--column", type=int,
                        help="Specify number of colums 1 to 5")
    args = parser.parse_args()
    if len(sys.argv) != 5:
        print('Not enough arguments, please call main.py with -h to see more')
        exit()
#    elif args.column > 5:
#        print('Number of columns must be between 1 and 5.. exiting')
#        exit()
#       print ('arguments ',   (sys.argv))

    lines = int(args.lines)
#    columns = int(args.column)
    file = args.output
    return lines, file


def openfile():
    pass


def closefile():
    pass


def looper(lines, outfile):

    file = open(outfile, "a")
    for x in range(0, lines):
        logline = '{}   {}  {}    {}    {}    {}    {}  {}\n'.format(genekas.gentimestamp(), genekas.genipv4(), genekas.genuser(), genekas.gendata(),
                                                                     genekas.genemail(), genekas.genurl(), genekas.gencommand(), genekas.gencode())
        file.write(logline)

    file.close()


# commented out since using python version ran into random errors..
# print('You are running on ', python_version())
lines, outfile = checkarg()
looper(lines, outfile)
print('exited with generating ' + str(lines) + ' lines')
